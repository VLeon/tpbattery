package com.example.scott.tp_noted.Models;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.scott.tp_noted.Services.BatteryService;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.scott.tp_noted.MainActivity.api;

public class BatteryReceiver extends BroadcastReceiver {


    public final String POWER_CONNECTED = "android.intent.action.ACTION_POWER_CONNECTED";
    public final String POWER_DISCONNECTED = "android.intent.action.ACTION_POWER_DISCONNECTED";

    private static String url = "https://battery-532ab.firebaseio.com";
    private BatteryService batteryService;


    @Override
    public void onReceive(final Context context, Intent intent) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://battery-532ab.firebaseio.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        batteryService = retrofit.create(BatteryService.class);


        Battery battery = new Battery();
        if (intent.getAction().equals(POWER_CONNECTED)) 	{
            Log.e("connection", "############## CONNECTED #########");
            InsertBattery(battery, "CONNECTED");
        }
        else if (intent.getAction().equals(POWER_DISCONNECTED)){
            Log.e("disconnected", "######## DISCONNECTED #######");
            InsertBattery(battery, "DISCONNECTED");
        }
    }

    private void InsertBattery(Battery battery, String action){
        battery.setAction(action);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        battery.setCurrentDate(formatter.format(new Date()));

        batteryService.createBattery(battery).enqueue(new Callback<Battery>() {
            @Override
            public void onResponse(Call<Battery> call, Response<Battery> response) {
                Log.i("info", "Statut enregistré");
            }

            @Override
            public void onFailure(Call<Battery> call, Throwable t) {

            }
        });
    }

}
