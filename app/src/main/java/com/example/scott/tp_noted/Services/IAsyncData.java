package com.example.scott.tp_noted.Services;

import java.util.Map;

public interface IAsyncData {

    void getData(Map<String, Double> myBatteries);

}
