package com.example.scott.tp_noted.Views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.example.scott.tp_noted.DetailActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class GraphView extends View {

    private Paint container = new Paint();
    private Paint bar = new Paint();
    private float width;
    private float height;

    private float widthBar;
    private float heightBar;

    private Rect rect;
    private List<Rect> rects = new ArrayList<>();

    private Map<String, Double> batteriesAsync = new HashMap<>();

    public GraphView(Context context) {
        super(context);
    }

    public GraphView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public GraphView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public GraphView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setBatteriesAsync(Map<String, Double> batteriesAsync) {
        this.batteriesAsync = batteriesAsync;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();

        switch (action) {
            case (MotionEvent.ACTION_DOWN):
                //invalidate();
                for (Rect rect: rects) {
                    if (rect.contains((int) event.getX(), (int) event.getY()))
                    {
                        Intent intent = new Intent(getContext(), DetailActivity.class);
                        getContext().startActivity(intent);
                    }
                }

            default:
                return super.onTouchEvent(event);
        }
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        width = getWidth();
        height = getHeight();
        heightBar = height / 24;

        widthBar = width / 7;
        int cpt = 0;

        Iterator it = batteriesAsync.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            double hour = (double)pair.getValue() / 3600;
            if (hour > 5) {
                bar.setColor(Color.RED);
            }
            else {
                bar.setColor(Color.GREEN);
            }
            bar.setStyle(Paint.Style.FILL);
            bar.setStrokeWidth(2);
            float leftValue = 35 + cpt * widthBar;
            canvas.drawRect(leftValue, height, leftValue + widthBar, height - (float) (hour * height / 24), bar);
            rects.add(new Rect((int)leftValue, (int)height, (int) ((int)leftValue + widthBar), (int)(height - (float) (hour * heightBar / 24))));
            cpt++;
            it.remove();
        }



    }


}
