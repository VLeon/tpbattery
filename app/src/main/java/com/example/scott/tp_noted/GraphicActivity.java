package com.example.scott.tp_noted;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.example.scott.tp_noted.Models.Battery;
import com.example.scott.tp_noted.Services.BatteryService;
import com.example.scott.tp_noted.Services.IAsyncData;
import com.example.scott.tp_noted.Views.GraphView;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GraphicActivity extends AppCompatActivity implements IAsyncData {

    private BatteryService batteryService;
    private static DecimalFormat df2 = new DecimalFormat(".##");
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat onlyDate = new SimpleDateFormat("dd/MM/yyyy");

    private Map<String, Double> secondsList = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graphic);

        Button exitButton = findViewById(R.id.btnExit);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://battery-532ab.firebaseio.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        batteryService = retrofit.create(BatteryService.class);

        getBatteries();

        exitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
    }

    private void getBatteries() {

        batteryService.getBattery().enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                List<Battery> batteries;
                if (response.isSuccessful() && response.body() != null) {
                    batteries = parseJson(response.body());
                    double seconds = 0;

                    for (int i = 0; i <= batteries.size() - 2; i++) {
                        try {
                            Date d1 =  onlyDate.parse(batteries.get(i).getCurrentDate());
                            //Date d2 =  onlyDate.parse(batteries.get(i+1).getCurrentDate());

                            Date d1Time = formatter.parse(batteries.get(i).getCurrentDate());
                            Date d2Time = formatter.parse(batteries.get(i+1).getCurrentDate());
                            seconds = (d2Time.getTime() - d1Time.getTime())/1000;
                            if (secondsList.containsKey(d1.toString())){
                                secondsList.put(d1.toString(), secondsList.get(d1.toString()) + seconds);
                            }
                            else {
                                secondsList.put(d1.toString(), seconds);
                            }

                            /*if (d1.compareTo(d2) == 0) {
                                Date d1Time = formatter.parse(batteries.get(i).getCurrentDate());
                                Date d2Time = formatter.parse(batteries.get(i+2).getCurrentDate());
                                seconds = (d2Time.getTime() - d1Time.getTime())/1000;
                                if (secondsList.containsKey(d1.toString())){
                                    secondsList.put(d1.toString(), secondsList.get(d1.toString() + seconds));
                                }
                                else {
                                    secondsList.put(d1.toString(), seconds);
                                }
                            }
                            else {

                            }*/
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    getData(secondsList);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("Get method", t.toString());
            }
        });
    }

    private List<Battery> parseJson(JsonObject obj) {
        List<Battery> batteries = new ArrayList<>();
        Gson gson = new Gson();
        Set<Map.Entry<String, JsonElement>> entrySet = obj.entrySet();
        for(Map.Entry<String, JsonElement> entry : entrySet) {
            batteries.add(gson.fromJson(obj.getAsJsonObject(entry.getKey()), Battery.class));
        }
        return batteries;
    }

    @Override
    public void getData(Map<String, Double> myBatteries) {
        GraphView graphView = findViewById(R.id.graphView);
        graphView.setBatteriesAsync(myBatteries);
        graphView.invalidate();
    }
}
