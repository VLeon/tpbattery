package com.example.scott.tp_noted.Services;

import android.annotation.SuppressLint;
import android.util.Log;

import com.example.scott.tp_noted.Models.Battery;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {

    private static Api api;
    private BatteryService batteryService;
    private static String url = "https://battery-532ab.firebaseio.com";
    private static DecimalFormat df2 = new DecimalFormat(".##");
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public static List<Battery> aBatteries = new ArrayList<>();


    private Api() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        batteryService = retrofit.create(BatteryService.class);
    }

    public static Api getInstance() {
        if (api == null) {
            api = new Api();
        }
        return api;
    }


    public void createBattery(Battery battery, Callback<Battery> callback) {
        Call<Battery> batteryCall = batteryService.createBattery(battery);
        batteryCall.enqueue(callback);
    }

    public void getBattery(){
        final Call<JsonObject> batteries = batteryService.getBattery();
        batteries.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                List<Battery> batteries;
                if (response.isSuccessful() && response.body() != null) {
                    batteries = parseJson(response.body());
                    double seconds = 0;

                    for (int i = 0; i < batteries.size() - 1; i++) {
                        try {
                            Log.i("count", String.valueOf(batteries.size()));
                            Date d1 =  formatter.parse(batteries.get(i).getCurrentDate());
                            Date d2 =  formatter.parse(batteries.get(i+1).getCurrentDate());

                            seconds += (d2.getTime()-d1.getTime())/1000;

                            Log.i("battery 1", d1.toString());
                            Log.i("battery 2", d2.toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    Log.i("temps", String.valueOf(Math.floor(seconds / 3600)) + "h");
                    Log.i("temps", String.valueOf(seconds) + "h");
                    /*for (Battery battery:
                            batteries) {

                        //Log.i("battery", battery.getAction() + " " + battery.getCurrentDate());
                    }*/
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("Get method", t.toString());
            }
        });


        /*Call<JsonObject> batteryCall = batteryService.getBattery();
        batteryCall.enqueue(callback);*/
    }

    private List<Battery> parseJson(JsonObject obj) {
        List<Battery> batteries = new ArrayList<>();
        Gson gson = new Gson();
        Set<Map.Entry<String, JsonElement>> entrySet = obj.entrySet();
        for(Map.Entry<String, JsonElement> entry : entrySet) {
            batteries.add(gson.fromJson(obj.getAsJsonObject(entry.getKey()), Battery.class));
        }
        return batteries;
    }

}
