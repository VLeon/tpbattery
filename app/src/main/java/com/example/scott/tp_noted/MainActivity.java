package com.example.scott.tp_noted;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.scott.tp_noted.Models.Battery;
import com.example.scott.tp_noted.Models.BatteryReceiver;
import com.example.scott.tp_noted.Services.Api;
import com.example.scott.tp_noted.Services.BatteryService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    public static Api api;
    private Button statButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        statButton = findViewById(R.id.btnStat);

        api = Api.getInstance();

        final String POWER_CONNECTED = "android.intent.action.ACTION_POWER_CONNECTED";
        final String POWER_DISCONNECTED = "android.intent.action.ACTION_POWER_DISCONNECTED";
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(POWER_CONNECTED);
        intentFilter.addAction(POWER_DISCONNECTED);
        BroadcastReceiver myBroadcast = new BatteryReceiver();
        getApplicationContext().registerReceiver(myBroadcast, intentFilter);

        statButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GraphicActivity.class);
                startActivity(intent);
            }
        });

    }
}
