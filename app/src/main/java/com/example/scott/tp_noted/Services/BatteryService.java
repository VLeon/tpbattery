package com.example.scott.tp_noted.Services;

import com.example.scott.tp_noted.Models.Battery;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface BatteryService {
    @GET("battery.json")
    Call<JsonObject> getBattery();

    @POST("battery.json")
    Call<Battery> createBattery(@Body Battery battery);
}