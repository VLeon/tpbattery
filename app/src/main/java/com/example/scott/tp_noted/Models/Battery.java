package com.example.scott.tp_noted.Models;

public class Battery {

    public Battery() { }

    private String currentDate;
    private String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String _currentDate) {
        this.currentDate = _currentDate;
    }
}
